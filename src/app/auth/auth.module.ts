import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import {AuthRoutingModule} from './auth.routing';
import {AppSharedModule} from '../shared/app-shared.module';

@NgModule({
  imports: [
    CommonModule, AuthRoutingModule, AppSharedModule
  ],
  declarations: [AuthComponent],
  providers: []
})
export class AuthModule { }
