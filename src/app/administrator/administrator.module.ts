import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdministratorComponent } from './administrator.component';
import {AppSharedModule} from '../shared/app-shared.module';
import {AdministratorRoutingModule} from './administrator.routing';

@NgModule({
  imports: [
    CommonModule, AppSharedModule, AdministratorRoutingModule
  ],
  declarations: [AdministratorComponent],
  providers: []
})
export class AdministratorModule { }
